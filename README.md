# AnyBite

Base64 private encoder

### Use: ###
Encode you string to Base64  
Add 15 random symbols to start and end base64 string  

In you php code:  

```echo anybite('RG8geW9=IGhhdmU0KLRg9C/0L4g0YLQtdC60YHRgg==HlvdXIg=ZGF0YS4');```